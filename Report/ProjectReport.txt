Project: chatonline

Member : 1.Thiraphat Wanachottrakul 6110210184 2.Sahasart Khumang 6110210428

Problem:  

When we study,Sometime we need to share a link or article but don't want to login facebook than we make the website chatonline

Proposal:   

chatonline is a website which allows everyone with link can chat in online and user don't have to register in this website 

Technology Used: 

- dotnet core 3.1 
- JavaScript
- SignalR
- Bootstrap

List of Features  (Prototype) 

- Can specify your name when chatting with others.
- If you don't enter your name is will replace by guest.
- Show chat history in font.

List of Features  (Complete Version) 
- Membership signing up 
- User can make a chat room by himself for Privacy.
- Can sent image or Video.
- Make a chat history file.